import Home from "./components/Home/Home";
import NewCustomer from "./components/NewBook/NewCustomer";
import Info from "./components/Info/Info"
import Edit from "./components/Edit/Edit";
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {
  //<Info type={'success'} description={'test'} />



  return (

    <BrowserRouter>
      <Routes>
        <Route  path="/" element={<Home/>} />
        <Route  path="/add" element={<NewCustomer/>} />
        <Route  path="/edit/:custId" element={<Edit/>} />
      </Routes>

    </BrowserRouter>
  )

}





export default App;
