import { useEffect, useState } from 'react';
import { addNewCustomer, createCustomer } from '../../service/api';
import Info from '../Info/Info';
import { useNavigate } from 'react-router-dom';



function NewCustomer() {

    const navigate = useNavigate();

    let [name, setName] = useState("");
    let [email, setEmail] = useState("");
    let [password, setPassword] = useState("");

    let [errors, setErrors] = useState([]);

    let [added, setAdded] = useState();


    let check=true;


   



    function checkValidation() {

        let aux=[];


        if (name =='') {


            aux.push("Name can't be empty")
           
        }

        if (email =='') {


            aux.push("Email can't be empty")
        

        }

        if (password =='') {
            
            aux.push("Password can't be empty")

        }
    
        setErrors([...aux]);

        if(aux.length>0){
            check=false;
        }else{
            check=true;
        }
    }





    async function addCustomer() {


        checkValidation();
        console.log("S-a trimis check validation")
        console.log(errors)
        
        if (check) {
            let newUser = {
                fullName: name,
                email,
                password
            }
            console.log(errors)
            let data = await createCustomer(newUser)

            console.log("S-a trimis CreateCustomer")
            console.log(data)

            setAdded(data);
            navigate("/")
        }
    }

    function cancelEdit(){
        navigate("/")
    }

    return (
        <>

            <h1>Add New Customer</h1>

            {
                errors.length > 0 && (
                    errors.map(err => {


                        return <Info type={'error'} description={err} />
                    })
                )
            }

            {
                added&&(
                    <Info type={added.type} description={added.desc} />
                )
            }
            <div className="form-container">
                <p>
                    <label for="name">Name</label>
                    <input name="Name" type="text" id="name" value={name} onChange={(event) => {
                        setName(event.target.value)
                    }} placeholder="Name" />
                </p>
                <p>
                    <label for="email">Email</label>
                    <input name="Email" type="text" id="email" value={email} onChange={(event) => {
                        setEmail(event.target.value)
                    }} placeholder="Email" />
                </p>
                <p>
                    <label for="password">Password</label>
                    <input name="Password" type="text" id="password" value={password} onChange={(event) => {
                        setPassword(event.target.value)
                    }} placeholder="Password" />
                </p>

                <p>
                    <a class="button" onClick={addCustomer}>
                        Save
                    </a>
                </p>
                <p>
                    <a class="button" onClick={cancelEdit}>Cancel</a>
                </p>
            </div>


        </>
    )
}

export default NewCustomer;