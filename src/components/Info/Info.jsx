import React from 'react';
import { Alert, Button, Space, message } from 'antd';

function Info({ description ,type }) {
  let messageNeeded = ""
  if(type=="success"){
    messageNeeded="Success"
  }else if (type==="error"){
    messageNeeded="Error"
  }
  return (
    <Alert
      message={messageNeeded}
      showIcon
      description={description}
      type={type}
      action={
        <Button size="small" danger>
          Detail
        </Button>
      }
      
    />  )
}









export default Info;