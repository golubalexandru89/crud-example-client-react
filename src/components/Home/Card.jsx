import { Link, useNavigate } from "react-router-dom";



function Card({ customer }) {

    let navigate=useNavigate();
    
    let editUser = () => {
        navigate(`/edit/${customer.id}`)

        //console.log(customer.id);
    }
    
    
    return (
        <tr>
            <td onClick={editUser}>
              
                 <Link to={`/edit/${customer.id}`}>{customer.id}</Link>
            </td>
            <td>{customer.fullName}</td>
            <td>{customer.email}</td>

        </tr>
    )
}

export default Card;